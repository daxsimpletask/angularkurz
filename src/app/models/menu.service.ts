import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class MenuService {

  goToPath(path,index){
    let pathArray = [];
    pathArray.push(path);
    this.router.navigate(pathArray);
    let self = this;
    this.deactivateAll(function(){
      self.menuLinks[index].active = true;
    });
    
  }

  deactivateAll(clbk){
    for(let i = 0; i < this.menuLinks.length; i++){
      this.menuLinks[i].active = false;
    }
    clbk()
  }

  menuLinks = [
    {
      active: true,
      label: 'Home',
      path: '/'
    },
    {
      active: false,
      label: 'About',
      path: '/about'
    },
    {
      active: false,
      label: 'News',
      path: '/'
    },
    {
      active: false,
      label: 'Contact',
      path: '/'
    },
    {
      active: false,
      label: 'Links',
      path: '/'
    }
  ]

  constructor(public router:Router) {}



}
