import { Injectable } from '@angular/core';
import { PostsService } from '../services/posts.service';

@Injectable()
export class PostsModel {
  posts = [];

  constructor(private service:PostsService) { 
    service.getPosts().subscribe(posts => {
      this.posts = posts;
    })
  }

  

}
