import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../models/menu.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {

  constructor(private menuModel:MenuService) { }

  ngOnInit() {
  }

}
