import { Component, OnInit } from '@angular/core';
import { PostsModel } from '../../models/posts.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  constructor(private postsModel:PostsModel) { }

  ngOnInit() {
  }

}
