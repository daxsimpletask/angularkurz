import { Component, OnInit } from '@angular/core';
import { MenuService } from '../../../models/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
})
export class MenuComponent implements OnInit {

  constructor(private menuModel:MenuService) { }

  ngOnInit() {
  }

}