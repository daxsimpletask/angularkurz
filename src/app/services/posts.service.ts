import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Headers, RequestOptions } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class PostsService {
  constructor(private http: Http) {}

  getPosts(): any {
    return this.http
      .get("https://jsonplaceholder.typicode.com/posts")
      .map(res => res.json());
  }

  addPost(post): any {
    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post("https://jsonplaceholder.typicode.com/posts", post, options)
      .map(res => res.json());
  }

  getPost(id): any {
    return this.http
      .get("https://jsonplaceholder.typicode.com/posts" + id)
      .map(res => res.json());
  }

  deletePost(id): any {
    return this.http
      .delete("https://jsonplaceholder.typicode.com/posts" + id)
      .map(res => res.json());
  }

  editPost(Post): any {
    let headers = new Headers({ "Content-Type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    return this.http
      .post("https://jsonplaceholder.typicode.com/posts" + Post.id, Post, options)
      .map(res => res.json());
  }
}
