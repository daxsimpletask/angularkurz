import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from "@angular/http";
import { FormsModule } from "@angular/forms";

import { MenuService } from './models/menu.service';
import { PostsService } from './services/posts.service';
import { PostsModel } from './models/posts.model';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';
import { MenuComponent } from './components/header/menu/menu.component';
import { HeaderMenuLink } from './components/header/menu/menu.link';
import { AboutComponent } from './components/about/about.component';

const naseRute:Routes = [
  {path:'', component:HomeComponent},
  {path:'about', component:AboutComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    HomeComponent,
    FooterComponent,
    MenuComponent,
    HeaderMenuLink,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(naseRute),
    FormsModule,
    HttpModule
  ],
  providers: [
    MenuService,
    PostsModel,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
